import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LongestIncreasingSubsequenceArrays {

    public static void main(final String[] args) {

	final Scanner in = new Scanner(System.in);
	final int p = in.nextInt();

	for (int i = 0; i < p; i++) {
	    final int m = in.nextInt();
	    final int n = in.nextInt();

	    System.out.println(recursive(n, m, new ArrayList<>()));
	}
    }

    private static int recursive(final int values, final int count, final List<Integer> list) {

	int sum = 0;
	if (count == 0) {
	    // final StringBuilder s = new StringBuilder();
	    // for (final Integer v : list) {
	    // s.append(v);
	    // s.append(" ");
	    // }
	    // System.out.println(s.toString());
	    if (checkSubsequence(values, list)) {
		return 1;
	    }
	    return 0;
	}

	for (int i = 0; i < values; i++) {
	    list.add(i + 1);
	    sum += recursive(values, count - 1, list);
	    list.remove(list.size() - 1);
	}
	return sum;

    }

    private static boolean checkSubsequence(final int values, final List<Integer> list) {
	int k = 1;
	for (final Integer integer : list) {
	    if (integer == k) {
		k++;
	    }
	}
	if (k > values) {
	    return true;
	}
	return false;
    }

    private static int solve(final int m, final int n) {
	final int array[] = new int[n];
	for (int i = 0; i < array.length; i++) {
	    array[i] = i + 1;
	}

	if (m < n) {
	    return 0;
	}

	return 0;
    }
}
