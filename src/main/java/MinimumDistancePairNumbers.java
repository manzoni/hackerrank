
/*
 *
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MinimumDistancePairNumbers {
    public static void main(final String args[]) {

	final String input = "6 4 8 7 1 3 7 9 1 8 7 8 7 3 9 1";
	final Scanner in = new Scanner(input);
	// final Scanner in = new Scanner(System.in);

	final int n = in.nextInt();
	final int p = in.nextInt();

	final Map<Integer, List<Integer>> index = new HashMap<>();

	for (int i = 0; i < n; i++) {
	    final int value = in.nextInt();
	    putValue(value, i, index);
	}

	for (int i = 0; i < p; i++) {
	    final int x = in.nextInt();
	    final int y = in.nextInt();
	    final int distance = findMinimumDistance(index.get(x), index.get(y));
	    System.out.println(distance);
	}
    }

    private static int findMinimumDistance(final List<Integer> indexesX, final List<Integer> indexesY) {

	if (indexesX == null || indexesY == null) {
	    return -1;
	}

	int minimumDistance = Integer.MAX_VALUE;
	for (final Integer x : indexesX) {
	    for (final Integer y : indexesY) {
		final int distance = Math.abs(x - y);
		if (distance < minimumDistance) {
		    minimumDistance = distance;
		}
	    }
	}

	return minimumDistance;
    }

    private static void putValue(final Integer value, final Integer index, final Map<Integer, List<Integer>> indexes) {
	List<Integer> positions = indexes.get(value);
	if (positions == null) {
	    positions = new ArrayList<Integer>();
	}
	positions.add(index);
	indexes.put(value, positions);
    }
}