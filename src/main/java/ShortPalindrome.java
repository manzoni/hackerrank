import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ShortPalindrome {
    public static void main(final String[] args) {

	final Scanner in = new Scanner("kkkkkkkz");
	final String word = in.nextLine();

	final int result = solve(word);
	System.out.println(result);

    }

    private static int solve(final String word) {
	int sum = 0;
	for (int i = 0; i < word.length() - 3; i++) {
	    for (int j = i + 1; j < word.length() - 2; j++) {
		for (int k = j + 1; k < word.length() - 1; k++) {
		    for (int l = k + 1; l < word.length(); l++) {
			if (checkPalindrome(word, i, j, k, l)) {
			    sum++;
			}
		    }
		}
	    }
	}
	return sum;
    }

    private static int solve2(final String word) {
	final int sum = 0;
	final Map<Character, List<Integer>> sets = new HashMap<>();

	for (int i = 0; i < word.length(); i++) {
	    put(word.charAt(i), i, sets);
	}

	return sum;

    }

    private static void put(final char charAt, final int i,
	    final Map<Character, List<Integer>> sets) {

	List<Integer> list = sets.get(charAt);

	if (list == null) {
	    list = new ArrayList<>();
	}
	list.add(i);
    }

    private static boolean checkPalindrome(final String word, final int a,
	    final int b, final int c, final int d) {
	return word.charAt(a) == word.charAt(d)
		&& word.charAt(b) == word.charAt(c);
    }
}
