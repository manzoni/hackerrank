
/*
 *
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class StrongestPadlock {
    private static final String MESSAGE_NOT_ENOUGH_PADLOCKS = "Not enough padlocks with keys";

    static class Padlock implements Comparable<Padlock> {
	final private int id;
	final private int strength;

	public Padlock(final int id, final int strength) {
	    this.id = id;
	    this.strength = strength;
	}

	@Override
	public int compareTo(final Padlock another) {
	    if (this.strength < another.strength) {
		return -1;
	    }
	    return 1;
	}
    }

    static class Key {
	final private int id;

	public Key(final int id) {
	    this.id = id;
	}

	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + id;
	    return result;
	}

	@Override
	public boolean equals(final Object obj) {
	    if (this == obj) {
		return true;
	    }
	    if (obj == null) {
		return false;
	    }
	    if (getClass() != obj.getClass()) {
		return false;
	    }
	    final Key other = (Key) obj;
	    if (id != other.id) {
		return false;
	    }
	    return true;
	}

    }

    private static List<Padlock> strongestPadlocks(final int n, final List<Padlock> padlocks, final List<Key> keys) {
	Collections.sort(padlocks);
	Collections.reverse(padlocks);

	final List<Padlock> strongestPadlocks = new ArrayList<Padlock>();

	int spots = n;
	for (final Padlock padlock : padlocks) {
	    if (keys.contains(new Key(padlock.id))) {
		keys.remove(new Key(padlock.id));
		strongestPadlocks.add(padlock);
		spots--;
	    }
	    if (spots == 0) {
		break;
	    }
	}

	return strongestPadlocks;
    }

    private static int loadPadlocksAndKeys(final Scanner in, final List<Padlock> padlocks, final List<Key> keys) {
	final int p = in.nextInt();
	final int k = in.nextInt();
	final int n = in.nextInt();

	// load padlocks
	for (int i = 0; i < p; i++) {
	    final int padlockId = in.nextInt();
	    final int padlockStrength = in.nextInt();
	    final Padlock padlock = new Padlock(padlockId, padlockStrength);
	    padlocks.add(padlock);
	}

	// load keys
	for (int i = 0; i < k; i++) {
	    final int keyId = in.nextInt();
	    final Key key = new Key(keyId);
	    keys.add(key);
	}

	return n;
    }

    public static void main(final String args[]) {

	final String input = "5 3 3 1 15 2 20 4 5 4 22 5 58 4 5 4";
	final Scanner in = new Scanner(input);
	// final Scanner in = new Scanner(System.in);

	final List<Padlock> padlocks = new ArrayList<Padlock>();
	final List<Key> keys = new ArrayList<Key>();

	final int n = loadPadlocksAndKeys(in, padlocks, keys);

	final List<Padlock> strongestPadlocks = strongestPadlocks(n, padlocks, keys);

	if (strongestPadlocks.size() < n) {
	    System.out.println(MESSAGE_NOT_ENOUGH_PADLOCKS);
	} else {
	    for (final Padlock padlock : strongestPadlocks) {
		System.out.println(padlock.id);
	    }
	}

    }
}