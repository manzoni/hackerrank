/*
 *
 */
package bitmanipulation;

import java.util.Scanner;

public class FlippingBts
{
    public static void main(final String[] args)
    {
        final Scanner in = new Scanner(System.in);

        final int x = in.nextInt();
        for (int i = 0; i < x; i++)
        {
            System.out.println(~in.nextLong() & 0xffffffffL);
        }

    }
}
