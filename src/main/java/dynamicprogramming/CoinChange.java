package dynamicprogramming;
import java.util.Scanner;

public class CoinChange {

    public static void main(final String[] args) {
	final String x = "10 4 2 5 3 6";
	final Scanner in = new Scanner(x);
	// final Scanner in = new Scanner(System.in);
	final int amount = in.nextInt();
	final int coinsLenght = in.nextInt();
	final int coins[] = new int[coinsLenght];
	final int coinsBool[] = new int[50];
	for (int i = 0; i < coinsLenght; i++) {
	    final int coin = in.nextInt();
	    coins[i] = coin;
	    coinsBool[coin] = 1;
	}
	System.out.println(solveProblem(amount, coins));
    }

    private static int solveProblem(final int amount, final int[] coins) {
	if (amount == 0) {
	    return 1;
	}
	int sum = 0;
	for (int i = 0; i < coins.length; i++) {
	    if (coins[i] != amount) {
		final int quociente = amount / coins[i];
		if (quociente != 0) {
		    final int resto = amount % coins[i];
		    if (resto != 0) {
			sum += solveProblem(resto, coins);
		    } else {
			sum++;
			sum += solveProblem(coins[i], coins);
		    }
		}
	    } else {
		sum++;
	    }
	}
	return sum;
    }
}
