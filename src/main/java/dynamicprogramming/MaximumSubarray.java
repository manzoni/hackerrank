package dynamicprogramming;
import java.util.Scanner;

public class MaximumSubarray
{

    public static void main(final String[] args)
    {
        final Scanner in = new Scanner(System.in);
        final int cases = in.nextInt();
        for (int i = 0; i < cases; i++)
        {
            final int arrayLenght = in.nextInt();
            final int array[] = new int[arrayLenght];
            for (int j = 0; j < arrayLenght; j++)
            {
                array[j] = in.nextInt();
            }
            final int result[] = solveProblem(array);
            System.out.println(result[0] + " " + result[1]);
        }
    }

    private static int[] solveProblem(final int[] array)
    {
        if (array.length == 1)
        {
            return new int[] { array[0], array[0] };
        }

        int sumSequence = Integer.MIN_VALUE;
        int sumSoFarSequence = 0;
        int sumNoSequence = 0;

        int maxValue = Integer.MIN_VALUE;

        for (final int element : array)
        {
            maxValue = Math.max(element, maxValue);
            sumSoFarSequence = Math.max(0, sumSoFarSequence + element);
            if (sumSoFarSequence == 0)
            {
                sumSequence = Math.max(sumSequence, maxValue);
            }
            else
            {
                sumSequence = Math.max(sumSequence, sumSoFarSequence);
            }
            if (element > 0)
            {
                sumNoSequence += element;
            }
        }

        if (maxValue < 0)
        {
            sumNoSequence = maxValue;
        }

        return new int[] { sumSequence, sumNoSequence };
    }
}