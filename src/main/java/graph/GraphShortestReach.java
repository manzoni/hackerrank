package graph;
import java.util.HashSet;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

public class GraphShortestReach {

    public static void main(final String[] args) {
	// final String x =
	// "3 4 2 1 2 1 3 1 3 1 2 3 2 7 7 1 2 1 3 1 4 4 5 4 6 6 7 3 7 4";
	// final Scanner in = new Scanner(x);
	final Scanner in = new Scanner(System.in);
	final int cases = in.nextInt();
	for (int i = 0; i < cases; i++) {
	    final int nodes = in.nextInt();
	    final int edges = in.nextInt();
	    final int graph[][] = new int[nodes][nodes];
	    for (int j = 0; j < edges; j++) {
		final int a = in.nextInt() - 1;
		final int b = in.nextInt() - 1;
		graph[a][b] = 1;
		graph[b][a] = 1;
	    }
	    final int start = in.nextInt() - 1;
	    print(solveProblem(graph, start), start);
	}
    }

    private static void print(final int[] solveProblem, final int start) {
	final StringBuffer s = new StringBuffer();
	for (int i = 0; i < solveProblem.length; i++) {
	    if (i != start) {
		if (solveProblem[i] == 0) {
		    s.append("-1 ");
		} else {
		    s.append(solveProblem[i] + " ");
		}
	    }
	}
	System.out.println(s.toString());
    }

    private static int[] solveProblem(final int[][] graph, final int startNode) {
	final Queue<Integer> queue = new LinkedBlockingQueue<>();
	queue.add(startNode);
	final int path[] = new int[graph.length];
	final Set<Integer> already = new HashSet<Integer>();
	while (!queue.isEmpty()) {
	    final int node = queue.poll();
	    already.add(node);
	    for (int i = 0; i < graph.length; i++) {
		if (graph[node][i] == 1 && !already.contains(i)) {
		    path[i] = path[node] + 6;
		    queue.add(i);
		    already.add(i);
		}
	    }
	}
	return path;
    }
}
