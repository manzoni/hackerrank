/*
 *
 */
package greedy;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class BeautifulPairs
{
    public static void main(final String[] args) throws FileNotFoundException
    {
        // final String x = "1 1 1";
        // final Scanner in = new Scanner(x);
        final Scanner in = new Scanner(System.in);
        final int k = in.nextInt();
        final int a[] = new int[k];
        final int b[] = new int[k];

        for (int i = 0; i < k; i++)
        {
            a[i] = in.nextInt();
        }
        for (int i = 0; i < k; i++)
        {
            b[i] = in.nextInt();
        }
        final int result = solveProblem(a, b);
        System.out.println(result);
    }

    private static int solveProblem(final int[] a, final int[] b)
    {
        int result = 0;
        for (final int element : a)
        {
            for (int j = 0; j < b.length; j++)
            {
                if (element == b[j])
                {
                    b[j] = -1;
                    result++;
                    break;
                }
            }
        }

        final int copy = result;
        for (final int element : b)
        {
            if (element > 0)
            {
                result++;
                break;
            }
        }
        if (copy == result)
        {
            return result - 1;
        }

        return result;
    }
}
