package greedy;
import java.util.Scanner;

public class SherlockAndTheBeast {
    public static void main(final String[] args) {
	// final String x = "6 10 1 2 11 9 17";
	// final Scanner in = new Scanner(x);
	final Scanner in = new Scanner(System.in);
	final int k = in.nextInt();
	for (int i = 0; i < k; i++) {
	    final int n = in.nextInt();
	    final String result = solveProble(n);
	    System.out.println(result);
	}
    }

    private static String solveProble(final int n) {

	int sizeFive = n % 3;

	final char[] result = new char[n];
	final String total = new String(result).replace('\0', '5');
	if (n % 3 == 0) {
	    return total;
	} else {
	    int sizeThree = 5;
	    if (sizeThree > n) {
		return "-1";
	    }
	    sizeFive = n - 5;
	    while ((sizeFive % 3 != 0 || sizeThree % 5 != 0) && sizeThree <= n) {
		sizeFive--;
		sizeThree++;
	    }
	    if (sizeThree > n) {
		return "-1";
	    }
	    return createValue(sizeFive, sizeThree);

	}
    }

    private static String createValue(final int maxFive, final int maxThree) {
	final StringBuffer buffer = new StringBuffer();
	for (int i = 0; i < maxFive; i++) {
	    buffer.append(5);
	}
	for (int i = 0; i < maxThree; i++) {
	    buffer.append(3);
	}
	return buffer.toString();
    }

}