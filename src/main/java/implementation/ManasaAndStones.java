/*
 *
 */
package implementation;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ManasaAndStones {
    public static void main(final String[] args) throws FileNotFoundException {
	//
	// final String x = "2 3 1 2 4 10 100";
	// final Scanner in = new Scanner(x);
	final Scanner in = new Scanner(System.in);

	final int cases = in.nextInt();
	for (int i = 0; i < cases; i++) {
	    final int stones = in.nextInt();
	    final int a = in.nextInt();
	    final int b = in.nextInt();
	    // final Set<Integer> result = new TreeSet<>();
	    final String result = solveProblem(stones, a, b);
	    // result.forEach(k -> System.out.printf(k + " "));
	    System.out.println(result);
	}
    }

    // eh mais elegante mas demora:
    private static int solveProblem(final int stones, final int value, final int a, final int b, final Set<Integer> result) {
	if (stones < 1) {
	    result.add(value);
	    return value;
	}

	return solveProblem(stones - 1, a + value, a, b, result) + solveProblem(stones - 1, b + value, a, b, result);
    }

    private static String solveProblem(final int stones, final int a, final int b) {

	int sum = 0;

	int x;
	int y;
	if (a < b) {
	    x = a;
	    y = b;
	} else {
	    x = b;
	    y = a;
	}
	for (int i = 0; i < stones - 1; i++) {
	    sum += x;
	}

	final Set<Integer> set = new HashSet<Integer>();
	set.add(sum);
	final StringBuilder s = new StringBuilder();
	s.append(sum);
	s.append(" ");
	for (int i = 0; i < stones - 1; i++) {
	    sum += -x + y;
	    if (set.add(sum)) {
		s.append(sum);
		s.append(" ");
	    }
	}
	return s.toString();
    }
}
