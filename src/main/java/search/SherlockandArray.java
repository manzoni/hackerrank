package search;

/*
 *
 */
import java.util.Scanner;

public class SherlockandArray
{
    public static void main(final String[] args)
    {
        final Scanner in = new Scanner(System.in);

        final int cases = in.nextInt();
        for (int i = 0; i < cases; i++)
        {
            final int arrayLenght = in.nextInt();
            final int array[] = new int[arrayLenght];
            for (int j = 0; j < arrayLenght; j++)
            {
                array[j] = in.nextInt();
            }
            final String result = solveProblem(array);
            System.out.println(result);
        }
    }

    private static String solveProblem(final int[] array)
    {

        int i = 0;
        int j = array.length - 1;
        int leftSum = 0;
        int rightSum = 0;

        while (i < j)
        {
            if (leftSum < rightSum)
            {
                leftSum += array[i++];
            }
            else
            {
                rightSum += array[j--];
            }
        }
        if (leftSum == rightSum)
        {
            return "YES";
        }
        return "NO";
    }

}
