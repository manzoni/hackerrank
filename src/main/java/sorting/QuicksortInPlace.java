package sorting;
import java.util.Scanner;

public class QuicksortInPlace {

    public static void main(final String[] args) {
	// final String x = "7 1 3 9 8 2 7 5";
	// final Scanner in = new Scanner(x);
	final Scanner in = new Scanner(System.in);
	final int lenght = in.nextInt();
	final int array[] = new int[lenght];
	for (int i = 0; i < lenght; i++) {
	    array[i] = in.nextInt();
	}
	solveProblem(array, 0, array.length - 1);
    }

    private static void print(final int array[]) {
	final StringBuffer line = new StringBuffer();
	for (int i = 0; i < array.length; i++) {
	    line.append(array[i] + " ");
	}
	System.out.println(line.toString());
    }

    private static void solveProblem(final int[] array, final int lo, final int hi) {
	if (lo < hi) {
	    final int pivot = pivot(array, lo, hi);
	    print(array);
	    solveProblem(array, lo, pivot - 1);
	    solveProblem(array, pivot + 1, hi);
	}
    }

    private static int pivot(final int[] array, final int lo, final int hi) {
	int i = lo;
	// final int j = lo;

	for (int j = lo; j < hi; j++) {
	    if (array[j] < array[hi]) {
		final int aux = array[i];
		array[i] = array[j];
		array[j] = aux;
		i++;
	    }
	}
	final int aux = array[i];
	array[i] = array[hi];
	array[hi] = aux;

	return i;
    }

}
