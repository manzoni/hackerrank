/*
 *
 */
package strings;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

public class RichieRich {
    public static void main(final String[] args) throws FileNotFoundException {
	final Scanner in = new Scanner(new File("/home/rmanzoni/Documents/input29.txt"));
	// final Scanner in = new Scanner("6 5 192123457681754321119");
	// final Scanner in = new Scanner("6 3 092282");
	// final Scanner in = new Scanner(System.in);
	final int n = in.nextInt();
	final int k = in.nextInt();
	final String number = in.next();

	final String result = solve(n, k, number);
	System.out.println(result);
    }

    private static String solve(final int n, final int k, final String number) {

	final Queue<Integer> indexes = checkPalindrome2(number);

	if (k == 0 && indexes.size() == 0) {
	    return number;
	}

	final char[] value = number.toCharArray();

	int sizeValue = indexes.size();
	int allowChanges = k;
	if (sizeValue > k) {
	    return "-1";
	} else if (sizeValue == k) {
	    for (final Integer index : indexes) {
		change(value, value.length, index);
	    }

	    allowChanges = 0;
	} else if (k >= number.length()) {
	    return new String(new char[number.length()]).replace('\0', '9');
	} else if (indexes.isEmpty()) {
	    int current = 0;
	    while (allowChanges > 1 && current < number.length() / 2) {
		allowChanges -= changeToNine(value, value.length, current++);
	    }
	} else {
	    Integer current = 0;
	    while (allowChanges > 0) {
		sizeValue = indexes.size();
		if (allowChanges - sizeValue > 1) {
		    if (indexes.peek() == current) {
			indexes.poll();
		    }
		    final Integer changes = changeToNine(value, value.length, current++);
		    allowChanges -= changes;
		} else if (allowChanges == sizeValue) {
		    while (!indexes.isEmpty()) {
			change(value, value.length, indexes.poll());
			allowChanges--;
		    }
		} else if (allowChanges - sizeValue == 1) {
		    if (indexes.isEmpty()) {
			break;
		    }
		    final Integer changes = changeToNine(value, value.length, indexes.poll());
		    allowChanges -= changes;
		}
	    }
	}

	if (allowChanges > 0 && number.length() % 2 != 0) {
	    value[number.length() / 2] = '9';
	}

	return new String(value);
    }

    private static Integer changeToNine(final char[] value, final int length, final Integer index) {

	int changes = 0;
	if (Character.getNumericValue(value[index]) != 9) {
	    value[index] = '9';
	    changes++;
	}
	if (Character.getNumericValue(value[length - index - 1]) != 9) {

	    value[length - index - 1] = '9';
	    changes++;
	}
	return changes;
    }

    private static int change(final int last, final char[] value) {
	int result = -1;

	for (int i = last; i < value.length / 2; i++) {
	    if (Character.getNumericValue(value[i]) < 9) {
		value[i] = value[value.length - i - 1] = '9';
		result = i + 1;
		break;
	    }
	}

	return result;
    }

    private static void change(final char[] value, final int sizeValue, final Integer index) {
	if (Character.getNumericValue(value[index]) > Character.getNumericValue(value[sizeValue - index - 1])) {
	    value[sizeValue - index - 1] = value[index];
	} else {
	    value[index] = value[sizeValue - index - 1];
	}
    }

    private static List<Integer> checkPalindrome(final String number) {
	if (number.length() % 2 != 0) {
	    return null;
	}

	final int max = number.length();
	final List<Integer> indexes = new ArrayList<Integer>();
	for (int i = 0; i < max / 2; i++) {
	    if (number.charAt(i) != number.charAt(max - i - 1)) {
		indexes.add(i);
	    }
	}
	return indexes;
    }

    private static Queue<Integer> checkPalindrome2(final String number) {

	final Queue<Integer> queue = new LinkedBlockingQueue<Integer>();

	final int max = number.length();
	int end;
	// if (number.length() % 2 != 0) {
	// end = max / 2 + 1;
	// } else {
	end = max / 2;
	// }
	for (int i = 0; i < end; i++) {
	    if (number.charAt(i) != number.charAt(max - i - 1)) {
		queue.add(i);
	    }
	}
	return queue;
    }
}
